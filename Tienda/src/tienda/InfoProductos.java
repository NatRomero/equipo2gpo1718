/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tienda;

/**
 *
 * @author dav
 */
public class InfoProductos {
    
    String producto;
            float precio;
    int cantidad;

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public InfoProductos() {
    }

    public InfoProductos(String producto, int precio, int cantidad) {
        this.producto = producto;
        this.precio = precio;
        this.cantidad = cantidad;
        
    }

    @Override
    public String toString() {
        return " Producto: " + producto+ " Precio: " + precio + " Cantidad: " + cantidad;
    }
    
    
}
